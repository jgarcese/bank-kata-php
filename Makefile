init: install-hooks ## Initialize project
	docker-compose build --pull --no-cache
	docker-compose up --force-recreate --remove-orphans -d

up: ## Up all services
	docker-compose up -d

terminal: ## Enter the shell of php container
	docker exec -it bank-kata.api sh

test: ## Run test suites
	docker exec bank-kata.api ./vendor/bin/phpunit --configuration /app/phpunit.xml.dist
