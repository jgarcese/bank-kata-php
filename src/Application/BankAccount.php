<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Common\Money;
use App\Domain\Deposit\DepositService;
use App\Domain\Statement\Statement;
use App\Domain\Statement\StatementService;
use App\Domain\Withdrawal\WithdrawalService;

class BankAccount
{
    private DepositService $depositService;
    private WithdrawalService $withdrawalService;
    private StatementService $statementService;

    public function __construct(
        DepositService $depositService,
        WithdrawalService $withdrawalService,
        StatementService $statementService
    ) {
        $this->depositService = $depositService;
        $this->withdrawalService = $withdrawalService;
        $this->statementService = $statementService;
    }

    public function deposit(Money $amount)
    {
        $this->depositService->makeDeposit($amount);
    }

    public function withdraw(Money $amount)
    {
        $this->withdrawalService->makeWithdrawal($amount);
    }

    public function getStatement(): Statement
    {
        return $this->statementService->get(
            $this->depositService->getDeposits(),
            $this->withdrawalService->getWithdrawals()
        );
    }
}
