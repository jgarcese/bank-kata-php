<?php

declare(strict_types=1);

namespace App\Domain\Withdrawal;

interface WithdrawalRepository
{
    public function save(Withdrawal $withdrawal): void;

    public function findAll(): array;
}
