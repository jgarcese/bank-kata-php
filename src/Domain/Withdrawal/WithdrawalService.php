<?php

declare(strict_types=1);

namespace App\Domain\Withdrawal;

use App\Domain\Common\Money;
use App\Domain\Common\TimeServer;

class WithdrawalService
{
    private WithdrawalRepository $withdrawalRepository;
    private TimeServer $timeServer;

    public function __construct(
        WithdrawalRepository $withdrawalRepository,
        TimeServer $timeServer
    ) {
        $this->withdrawalRepository = $withdrawalRepository;
        $this->timeServer = $timeServer;
    }

    public function makeWithdrawal(Money $amount): void
    {
        $this->withdrawalRepository->save(new Withdrawal($amount, $this->timeServer->getDate()));
    }

    public function getWithdrawals(): array
    {

    }
}
