<?php

declare(strict_types=1);

namespace App\Domain\Withdrawal;

use App\Domain\Common\Money;
use App\Domain\Common\TransactionDate;

class Withdrawal
{
    private Money $amount;
    private TransactionDate $date;

    public function __construct(Money $amount, TransactionDate $date)
    {
        $this->amount = $amount;
        $this->date = $date;
    }

    public function date(): string
    {
        return $this->date->date();
    }
}
