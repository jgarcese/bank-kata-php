<?php

declare(strict_types=1);

namespace App\Domain\Common;

interface TimeServer
{
    public function getDate(): TransactionDate;
}
