<?php

declare(strict_types=1);

namespace App\Domain\Common;

class TransactionDate
{
    private $date;

    public function __construct(string $date)
    {
        $this->date = \DateTimeImmutable::createFromFormat('d/m/Y H:i:s', $date);
    }

    public function date(): string
    {
       return $this->date->format('d/m/Y H:i:s');
    }
}
