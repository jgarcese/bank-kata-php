<?php

declare(strict_types=1);

namespace App\Domain\Common;

class Money
{
    private int $amount;

    public function __construct(int $amount)
    {
        $this->setAmount($amount);
    }

    private function setAmount(int $amount)
    {
        if ($amount < 0) {
           $amount = 0;
        }

        $this->amount = $amount;
    }
}
