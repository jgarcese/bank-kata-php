<?php

declare(strict_types=1);

namespace App\Domain\Deposit;

interface DepositRepository
{
    public function save(Deposit $deposit): void;

    public function findAll(): array;
}
