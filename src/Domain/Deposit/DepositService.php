<?php

declare(strict_types=1);

namespace App\Domain\Deposit;

use App\Domain\Common\Money;
use App\Domain\Common\TimeServer;

class DepositService
{
    private DepositRepository $depositRepository;
    private TimeServer $timeServer;

    public function __construct(
        DepositRepository $depositRepository,
        TimeServer $timeServer
    ) {
        $this->depositRepository = $depositRepository;
        $this->timeServer = $timeServer;
    }

    public function makeDeposit(Money $amount): void
    {
        $this->depositRepository->save(new Deposit($amount, $this->timeServer->getDate()));
    }

    public function getDeposits(): array
    {
        return $this->depositRepository->findAll();
    }
}
