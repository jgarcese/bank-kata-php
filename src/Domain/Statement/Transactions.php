<?php

declare(strict_types=1);

namespace App\Domain\Statement;

class Transactions
{
    private array $transactions = [];

    public function addDeposit($deposit)
    {
       array_push($this->transactions, ['deposit' => $deposit]);
    }

    public function addWithdrawal($withdrawal)
    {
        array_push($this->transactions, ['withdrawal' => $withdrawal]);
    }

    public function sortByDate()
    {
        foreach ($this->transactions as $transaction => $transactionType) {
            if ($this->isDeposit($transactionType)) {
                $sort[$transaction] = strtotime($transactionType['deposit']->date());
                continue;
            }
            $sort[$transaction] = strtotime($transactionType['withdrawal']->date());
        }
        array_multisort($sort, SORT_DESC, $this->transactions);
    }

    private function isDeposit($transactionType): bool
    {
        return isset($transactionType['deposit']);
    }
}
