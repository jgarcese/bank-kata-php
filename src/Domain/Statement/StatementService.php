<?php

declare(strict_types=1);

namespace App\Domain\Statement;

class StatementService
{
    public function get(array $deposits, array $withdrawals): Statement
    {
        return new Statement($deposits, $withdrawals);
    }
}
