<?php

declare(strict_types=1);

namespace App\Domain\Statement;

class Statement
{
    private Transactions $transactions;

    public function __construct(array $deposits, array $withdrawals)
    {
        $this->transactions = new Transactions();
        $this->storeDeposits($deposits);
        $this->storeWithdrawals($withdrawals);
        $this->transactions->sortByDate();
    }

    private function storeDeposits(array $deposits)
    {
       foreach($deposits as $deposit) {
           $this->transactions->addDeposit($deposit);
       }
    }
    private function storeWithdrawals(array $withdrawals)
    {
        foreach($withdrawals as $withdrawal) {
            $this->transactions->addWithdrawal($withdrawal);
        }
    }
}
