<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Deposit\Deposit;
use App\Domain\Deposit\DepositRepository;

class InMemoryDepositRepository implements DepositRepository
{
    private array $deposits;

    public function __construct()
    {
        $this->deposits = [];
    }

    public function save(Deposit $deposit): void
    {
        $this->deposits[] = $deposit;
    }

    public function findAll(): array
    {
        return $this->deposits;
    }
}
