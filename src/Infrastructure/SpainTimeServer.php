<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Common\TimeServer;
use App\Domain\Common\TransactionDate;

class SpainTimeServer implements TimeServer
{
    public function getDate(): TransactionDate
    {
        date_default_timezone_set('Europe/Madrid');
        return new TransactionDate(date('y/m/Y H:i:s'));
    }
}
