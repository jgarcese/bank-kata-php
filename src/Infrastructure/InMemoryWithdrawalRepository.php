<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Withdrawal\Withdrawal;
use App\Domain\Withdrawal\WithdrawalRepository;

class InMemoryWithdrawalRepository implements WithdrawalRepository
{
    private array $withdrawals;

    public function __construct()
    {
        $this->withdrawals = [];
    }

    public function save(Withdrawal $withdrawal): void
    {
        $this->withdrawals[] = $withdrawal;
    }

    public function findAll(): array
    {
        return $this->withdrawals;
    }
}
