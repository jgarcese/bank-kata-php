<?php

declare(strict_types=1);

namespace App\Tests\unit\Application;

use App\Application\BankAccount;
use App\Domain\Common\Money;
use App\Domain\Common\TransactionDate;
use App\Domain\Deposit\DepositService;
use App\Domain\Statement\Statement;
use App\Domain\Statement\StatementService;
use App\Domain\Withdrawal\WithdrawalService;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class BankAccountTest extends TestCase
{
    use ProphecyTrait;

    private $depositService;
    private $withdrawalService;
    private $statementService;
    private BankAccount $bankAccount;

    protected function setUp(): void
    {
        $this->depositService = $this->prophesize(DepositService::class);
        $this->withdrawalService = $this->prophesize(WithdrawalService::class);
        $this->statementService = $this->prophesize(StatementService::class);
        $this->bankAccount = $bankAccount = new BankAccount(
            $this->depositService->reveal(),
            $this->withdrawalService->reveal(),
            $this->statementService->reveal()
        );
        $this->statementService->get(Argument::cetera())
            ->willReturn(new Statement(
                    ['some date' => new TransactionDate('01/01/2002 00:00:00')],
                    ['some date' => new TransactionDate('02/01/2002 00:00:00')]
                )
            );
    }

    /** @test */
    public function shouldCallDepositServiceWithAnAmountOf1000(): void
    {
        $amount = new Money(1000);

        $this->bankAccount->deposit($amount);

        $this->depositService->makeDeposit($amount)->shouldHaveBeenCalled();
    }

    /** @test */
    public function shouldCallWithdrawalServiceWithAnAmountOf500(): void
    {
        $amount = new Money(500);

        $this->bankAccount->withdraw($amount);

        $this->withdrawalService->makeWithdrawal($amount)->shouldHaveBeenCalled();
    }

    /** @test */
    public function shouldCallStatementServiceWithCorrectData(): void
    {
        $this->depositService->getDeposits()->willReturn(['deposits']);
        $this->withdrawalService->getWithdrawals()->willReturn(['withdrawals']);

        $this->bankAccount->getStatement();

        $this->statementService->get(['deposits'], ['withdrawals'])->shouldHaveBeenCalled();
    }
}
