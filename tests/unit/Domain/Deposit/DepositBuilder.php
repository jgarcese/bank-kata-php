<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Deposit;

use App\Domain\Common\Money;
use App\Domain\Common\TransactionDate;
use App\Domain\Deposit\Deposit;

class DepositBuilder
{
    private int $amount;
    private string $date;

    private function __construct()
    {
        $this->amount = 666;
        $this->date = '01/01/2002 00:00:00';
    }

    public static function aDeposit(): self
    {
       return new self();
    }

    public function withAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function withDate(string $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function build(): Deposit
    {
      return new Deposit(
          new Money($this->amount),
          new TransactionDate($this->date)
      );
    }
}
