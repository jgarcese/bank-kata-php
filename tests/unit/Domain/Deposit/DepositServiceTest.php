<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Deposit;

use App\Domain\Common\Money;
use App\Domain\Common\TimeServer;
use App\Domain\Common\TransactionDate;
use App\Domain\Deposit\DepositRepository;
use App\Domain\Deposit\DepositService;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class DepositServiceTest extends TestCase
{
    use ProphecyTrait;

    private $depositRepository;
    private $timeServer;
    private DepositService $depositService;

    protected function setUp(): void
    {
        $this->depositRepository = $this->prophesize(DepositRepository::class);
        $this->timeServer = $this->prophesize(TimeServer::class);
        $this->depositService = $depositService = new DepositService(
            $this->depositRepository->reveal(),
            $this->timeServer->reveal()
        );
    }

    /** @test */
    public function shouldCallDepositRepositorySaveWithCorrectData(): void
    {
        $this->timeServer->getDate()->willReturn(new TransactionDate('01/01/2021 00:00:00'));
        $amount = new Money(100);

        $this->depositService->makeDeposit($amount);

        $expectedDeposit = DepositBuilder::aDeposit()
            ->withAmount(100)
            ->withDate('01/01/2021 00:00:00')
            ->build();
        $this->depositRepository->save($expectedDeposit)->shouldHaveBeenCalled();
    }

    /** @test */
    public function shouldCallDepositRepositoryForDeposits(): void
    {
        $this->depositRepository->findAll()->willReturn([]);

        $this->depositService->getDeposits();

        $this->depositRepository->findAll()->shouldHaveBeenCalled();
    }
}
