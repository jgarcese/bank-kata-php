<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Statement;

use App\Domain\Deposit\Deposit;
use App\Domain\Statement\Statement;
use App\Domain\Withdrawal\Withdrawal;

class StatementBuilder
{
    private array $deposits = [];
    private array $withdrawals = [];

    public static function aStatement(): self
    {
       return new self();
    }

    public function withDeposit(Deposit $deposit): self
    {
        $this->deposits[] = $deposit;
        return $this;
    }
    public function withWithdrawal(Withdrawal $withdrawal): self
    {
        $this->withdrawals[] = $withdrawal;
        return $this;
    }
    public function build(): Statement
    {
        return new Statement($this->deposits, $this->withdrawals);
    }
}
