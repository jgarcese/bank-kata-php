<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Statement;

use App\Domain\Statement\StatementService;
use App\Tests\unit\Domain\Deposit\DepositBuilder;
use App\Tests\unit\Domain\Withdrawal\WithdrawalBuilder;
use PHPUnit\Framework\TestCase;

class StatementServiceTest extends TestCase
{
    private StatementService $statementService;

    protected function setUp(): void
    {
        $this->statementService = $statementService = new StatementService();
    }

    /** @test */
    public function shouldReturnAStatementWithDepositsOrderedByDateDescending()
    {
        $depositOne = DepositBuilder::aDeposit()->withDate('01/01/2002 00:00:00')->build();
        $depositTwo = DepositBuilder::aDeposit()->withDate('02/01/2002 00:00:00')->build();
        $depositThree = DepositBuilder::aDeposit()->withDate('03/01/2002 00:00:00')->build();

        $deposits = [$depositTwo, $depositOne, $depositThree];
        $withdrawals = [];

        $resultingStatement = $this->statementService->get($deposits, $withdrawals);

        $expectedStatement = StatementBuilder::aStatement()
            ->withDeposit($depositTwo)
            ->withDeposit($depositThree)
            ->withDeposit($depositOne)
            ->build();
        self::assertEquals($expectedStatement, $resultingStatement);
    }

    /** @test */
    public function shouldReturnAStatementWithWithdrawalsAndDepositsOrderedByDateDescending()
    {
        $depositOne = DepositBuilder::aDeposit()->withDate('01/01/2002 00:00:00')->build();
        $withdrawalOne = WithdrawalBuilder::aWithdrawal()->withDate('02/01/2002 00:00:00')->build();
        $withdrawalTwo = WithdrawalBuilder::aWithdrawal()->withDate('03/01/2002 00:00:00')->build();

        $deposits = [$depositOne];
        $withdrawals = [$withdrawalOne, $withdrawalTwo];

        $resultingStatement = $this->statementService->get($deposits, $withdrawals);

        $expectedStatement = StatementBuilder::aStatement()
            ->withWithdrawal($withdrawalTwo)
            ->withWithdrawal($withdrawalOne)
            ->withDeposit($depositOne)
            ->build();
        self::assertEquals($expectedStatement, $resultingStatement);
    }
}
