<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Withdrawal;

use App\Domain\Common\Money;
use App\Domain\Common\TimeServer;
use App\Domain\Common\TransactionDate;
use App\Domain\Withdrawal\WithdrawalService;
use App\Domain\Withdrawal\WithdrawalRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class WithdrawalServiceTest extends TestCase
{
    use ProphecyTrait;

    /** @test */
    public function shouldCallWithdrawalRepositoryWithCorrectData(): void
    {
        $withdrawalRepository = $this->prophesize(WithdrawalRepository::class);
        $timeServer = $this->prophesize(TimeServer::class);
        $withdrawalService = new WithdrawalService(
            $withdrawalRepository->reveal(),
            $timeServer->reveal()
        );
        $timeServer->getDate()->willReturn(new TransactionDate('01/01/2021 00:00:00'));
        $amount = new Money(100);

        $withdrawalService->makeWithdrawal($amount);

        $expectedWithdrawal = WithdrawalBuilder::aWithdrawal()
            ->withAmount(100)
            ->withDate('01/01/2021 00:00:00')
            ->build();
        $withdrawalRepository->save($expectedWithdrawal)->shouldHaveBeenCalled();
    }
}
