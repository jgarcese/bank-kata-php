<?php

declare(strict_types=1);

namespace App\Tests\unit\Domain\Withdrawal;

use App\Domain\Common\Money;
use App\Domain\Common\TransactionDate;
use App\Domain\Withdrawal\Withdrawal;

class WithdrawalBuilder
{
    private int $amount;
    private string $date;

    private function __construct()
    {
        $this->amount = 666;
        $this->date = '01/01/2002 00:00:00';
    }

    public static function aWithdrawal(): self
    {
       return new self();
    }

    public function withAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function withDate(string $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function build(): Withdrawal
    {
      return new WithDrawal(
          new Money($this->amount),
          new TransactionDate($this->date)
      );
    }
}
