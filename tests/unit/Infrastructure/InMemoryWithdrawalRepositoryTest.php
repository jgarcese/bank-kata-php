<?php

declare(strict_types=1);

namespace App\Tests\unit\Infrastructure;

use App\Infrastructure\InMemoryWithdrawalRepository;
use App\Tests\unit\Domain\Withdrawal\WithdrawalBuilder;
use PHPUnit\Framework\TestCase;

class InMemoryWithdrawalRepositoryTest extends TestCase
{
    /** @test */
    public function shouldStoreADepositWithAmountAndDate(): void
    {
        $inMemoryWithdrawalRepository = new InMemoryWithdrawalRepository();
        $withdrawal = WithdrawalBuilder::aWithdrawal()
            ->withAmount(100)
            ->withDate('03/01/2021 00:00:00')
            ->build();

        $inMemoryWithdrawalRepository->save($withdrawal);

        self::assertEquals([$withdrawal], $inMemoryWithdrawalRepository->findAll());
    }

    private function save(\App\Domain\Deposit\Deposit $withdrawal)
    {
    }
}
