<?php

declare(strict_types=1);

namespace App\Tests\unit\Infrastructure;

use App\Infrastructure\InMemoryDepositRepository;
use App\Tests\unit\Domain\Deposit\DepositBuilder;
use PHPUnit\Framework\TestCase;

class InMemoryDepositRepositoryTest extends TestCase
{
    /** @test */
    public function shouldStoreADepositWithAmountAndDate(): void
    {
        $inMemoryDepositRepository = new InMemoryDepositRepository();
        $deposit = DepositBuilder::aDeposit()
            ->withAmount(100)
            ->withDate('03/01/2021 00:00:00')
            ->build();

        $inMemoryDepositRepository->save($deposit);

        self::assertEquals([$deposit], $inMemoryDepositRepository->findAll());
    }

    /** @test */
    public function shouldReturnEmptyArrayIfNoDepositsExist(): void
    {
        $inMemoryDepositRepository = new InMemoryDepositRepository();

        self::assertEquals([], $inMemoryDepositRepository->findAll());
    }
}
